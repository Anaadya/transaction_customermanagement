package com.npci.crudproject.dao.impl;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.npci.crudproject.dao.*;
import com.npci.crudproject.response.*;
import com.npci.crudproject.support.*;

@Repository
public class CustomerDaoImpl extends NameParameterJdbcDaoSupportclass implements CustomerDao {
	@Autowired JdbcTemplate jdbcTemplate;

	@Override
	public List<TransactionResponse> getTransDetail() {
		// TODO Auto-generated method stub
		List<TransactionResponse> transaction = null;
		try {
			String query = "select t.trans_id,t.trans_amount,t.from_name,t.to_name,t.trans_date , (c.balance + t.trans_amount) as Updated_Balance from Transactions t inner join customers c on t.to_c_id = c.c_id order by trans_id limit 2 offset 1;"
					;
			transaction = getNamedParameterJdbcTemplate()
					.getJdbcOperations()
					.query(
							query, 
							new BeanPropertyRowMapper<TransactionResponse>(
									TransactionResponse.class
									));
		} catch (Exception ex) {
			// TODO: handle exception
			ex.getStackTrace();
		}
		
		return transaction;
	}
	
	
}

