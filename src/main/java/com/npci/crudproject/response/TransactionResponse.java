package com.npci.crudproject.response;

public class TransactionResponse {

	private int trans_id;
	private String from_name;
	private String to_name;
	private int trans_amount;
	private int Updated_Balance;

	

	public TransactionResponse() {
		super();
	}

	public TransactionResponse(int trans_id, String from_name, int trans_amount, String to_name, int Updated_Balance) {
		super();
		this.trans_id = trans_id;
		this.from_name = (from_name);
		this.to_name = (to_name);
		this.trans_amount = trans_amount;
		this.Updated_Balance = Updated_Balance;
		
	}

	public int getTrans_id() {
		return trans_id;
	}

	public int getTrans_amount() {
		return trans_amount;
	}

	public void setTrans_amount(int trans_amount) {
		this.trans_amount = trans_amount;
	}

	public void setTrans_id(int trans_id) {
		this.trans_id = trans_id;
	}

	public String getFrom_name() {
		return from_name;
	}

	public void setFrom_name(String from_name) {
		this.from_name = from_name;
	}

	public String getTo_name() {
		return to_name;
	}

	public void setTo_name(String to_name) {
		this.to_name = to_name;
	}

	@Override
	public String toString() {
		return "TransactionResponse [trans_id=" + trans_id + ", from_name=" + from_name + ", to_name=" + to_name
				+ ", trans_amount=" + trans_amount + ", Updated_Balance=" + Updated_Balance + "]";
	}

	public int getUpdated_Balance() {
		return Updated_Balance;
	}

	public void setUpdated_Balance(int updated_Balance) {
		Updated_Balance = updated_Balance;
	}

	

	
}
