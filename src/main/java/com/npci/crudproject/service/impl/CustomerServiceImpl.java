package com.npci.crudproject.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.npci.crudproject.dao.*;
import com.npci.crudproject.entity.*;
import com.npci.crudproject.exception.ResourceNotFoundException;
import com.npci.crudproject.repository.*;
import com.npci.crudproject.response.*;
import com.npci.crudproject.service.*;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired CustomerDao customerDao;
	@Autowired CustomerRepository customerRepository;
	@Override
	public List<Customers> getAllCustomers() {
		// TODO Auto-generated method stub
		return (List<Customers>) customerRepository.findAll();
	}
	
	@Override
	public Customers getById(int c_id) {
		// TODO Auto-generated method stub
//		return customerRepository.findById(c_id).orElse(null);
		return customerRepository
				.findById(c_id)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not Found with c_id: " + c_id));
	}

	@Override
	public List<Customers> getbyLoc(String address) {
		// TODO Auto-generated method stub
		return (List<Customers>) customerRepository.getbyLoc(address);
	}

	@Override
	public Customers addCustomer(Customers customers) {
		// TODO Auto-generated method stub
		return customerRepository.save(customers);
	}

	@Override
	public String deleteCustomer(int c_id) {
		// TODO Auto-generated method stub
		Customers customer = null;
		String message = null;
		try {
			customer = customerRepository.findById(c_id).orElse(null);
			if(customer == null) {
				message = "Customer unavailable";
			}
			else {
				customerRepository.deleteById(c_id);
				message = "Deleted";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
		}
		
		return message;
	}

	@Override
	public List<TransactionResponse> getTransDetails() {
		// TODO Auto-generated method stub
		return (List<TransactionResponse>) customerDao.getTransDetail() ;
	}

}


